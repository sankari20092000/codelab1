PROGRAM :
=========
DayOneProgram.java
===================

package com.hcl.week1;

import java.util.Arrays;
import java.util.Scanner;

public class DayOneProgram {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		// As a user I should be able to print my name on the screen

		System.out.println("Enter Name:");

		String Name = scan.next();
		System.out.println(Name);

		// As a user I should be able to list at least 5 task for the day.

		String tasks[] = { "Programming", "Testing", "Developing", "Debuging", "Testing" };
		Arrays.toString(tasks);

		// As a user I should be able to see all the task in increasing and decreasing
		// order.

		System.out.println("Task in increasig order : ");

		for (int i = 0; i < 5; i++) {
			System.out.println(tasks[i]);

		}
		System.out.println("Task in decreasig order : ");

		for (int i = tasks.length - 1; i >= 0; i--) {
			System.out.println(tasks[i]);

		}
		// As a user I should be able to see the repeated tasks
		for(int i=0;i<tasks.length;i++)
		{
			for(int j =i+1;j<tasks.length;j++)
			{
				if(tasks[i]==tasks[j])
				{
					System.out.println("Reapeated Task : "+tasks[j]);
				}
			}
		}

	}

}

OUTPUT:
========
Enter Name:
karuna
karuna
Task in increasig order : 
Programming
Testing
Developing
Debuging
Testing
Task in decreasig order : 
Testing
Debuging
Developing
Testing
Programming
Reapeated Task : Testing
