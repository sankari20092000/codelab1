Question 1
============
Write a program in java to implement a basic calculator using switch case statement
Accept two no as user input
Display list of options 
        1 → addition
        2 → subtraction
        3 → multiplication
        4 → division

Perform division only if the second number is not equal to 0. If the second number is 0 print “Cannot be divided” on console.
OperatorProgram.java
=====================
PROGRAM
=========
package com.hcl.week1;
import java.util.Scanner;
public class OperatorProgram {

	public static void main(String[] args) {
		// Write a program in java to implement a basic calculator using switch case statement
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter Two Value : a & b");
		int a=scan.nextInt();
		int b=scan.nextInt();
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		if(b == 0)
		{
			System.out.println("Cannot be divided");
		}
		else {
			System.out.println(a/b);
		}
	}

}
OUTPUT
========
Enter Two Value : a & b
14
2
16
12
28
7

Question 2
==========
Write a program to interchange two numbers without using a third variable.

Sample Test Case
Input
10
20

InterChageProgram.java
=======================
PROGRAM
=========
package com.hcl.week1;

public class InterChangeProgram {

	public static void main(String[] args) {
		int a=10;
		int b=58;
		System.out.println("The Two Variable :" + a+" " + b);

		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After Iterchaging :" + a+" " + b);
		
	}

}

OUTPUT
=======
The Two Variable :10 58
After Iterchaging :58 10

Question 3

Write a program to print the following pattern. 
Sample Test Case
Input
5
Pattern.java
===========
PROGRAM
=========

package com.hcl.week1;
import java.util.Scanner;
class Pattern{
    static void printPattern(int num){
             int k=1;
	        for(int i=1;i<=num;i++)
	        {
	        	for(int j=1;j<=i;j++)
	        	{
	        		System.out.print(k++ +" ");
	        		
	        	}
	        	System.out.println();
	        }
	    }

	public static void main(String[] args) {

	Scanner in = new Scanner(System.in);
	System.out.println("Enter Value : ");
	int num = in.nextInt();

	printPattern(num);

	}

	}

OUTPUT
=======
Enter Value : 
5
1 
2 3 
4 5 6 
7 8 9 10 
11 12 13 14 15 

Question 4
Write a program to find reverse of a number
Sample Test Case 
Input
1234
PROGRAM
=========
package com.hcl.week1;
import java.io.*;
	  
public class ReverseProgram 
{
      // Function to reverse the number
	    static int reverse(int n){
	        
	      int rev = 0; // reversed number
	      int rem;   // remainder
	        
	      while(n>0){
	           
	        rem = n%10;
	        rev = (rev*10) + rem;
	        n = n/10;
	      }
	        
	      return rev;
	    }
	    

	    public static void main (String[] args)
	    {
	        int n = 1234;
	          
	        System.out.print("Reversed Number is "+ reverse(n));
	    }
}


OUTPUT
=======
Reversed Number is 4321